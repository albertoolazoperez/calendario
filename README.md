# Calendario

Calendario compatible con bootstrap 4 

Este calendario cumple con las functiones básicas.

Pasos:
1. Importar el archivo calendar.js
2. Llamamos la función apiCalendar.setCalendar("DivDestino", "urlAPI", "Mes (opcional)", "Año (opcional)")

La API deberá ser construida de tal manera que pueda recibir una variable month (GET) y una variable year (GET)
ejemplo: urlAPI = "http://ocalhost/api/eventos" automaticamente al asignar estos valores, no es necesario hacer referencia a las variables (GET) ya que estas se asignan dentro del método setCalendar() de manera automática.
Cabe mencionar que la API debera devolver objeto(s) JSON con dos propiedades (date/event).