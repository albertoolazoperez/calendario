
class apiCalendar{

    constructor(){
    }

    static clickDay(dayIndex, monthIndex, yearIndex) 
    {
        alert(dayIndex+'/'+monthIndex+'/'+yearIndex)
        setCalendar(auxDiv, auxMonth+1, year)
    }

    static getMonthDays(mes, año) 
    {
        return new Date(año, mes, 0).getDate();
    }

    static getWeekDay(day, month, year) 
    {
        var weekdays = ["D","L","M","X","J","V","S"]
        var dateTime = new Date(month+' '+day+' '+year+' 12:00:00')
        return weekdays[dateTime.getUTCDay()]
    }

    static getMonthName(month) 
    {
        var months = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]
        return months[month-1]
    }

    static setCalendar(divSet, urlAPI = "", actualMonth = 0, actualYear = 0)
    {

        var today = "14/8/2019"
        // Al url se le indicará un parametro de mes y otro año '?month = xx&year = xxxx' (deben ir indicados)
        if(urlAPI != ""){
            let fechas  = []
            let content = []
            
            $.ajax({
                url: urlAPI,
                method: ""+"?month="+actualMonth+"&year"+actualYear,
                dataTupe: "",
                success: function(result){
                    this.dataCalendar = result
                },
                error: function(e){
                    console.log("An error has encountered with the url: "+e)
                }
            })
            return this.dataCalendar
        }

        this.auxMonth = actualMonth
        this.auxDiv   = divSet
        this.year = actualYear
        document.getElementById(divSet).innerHTML = ""

        var nameMonth = this.getMonthName(actualMonth)

        var stringCalendar = '<div class="row">' 
            stringCalendar += '<div class="col-md-4 text-center"><a class="btn prev-btn" id="prevMonth" href="#">Prev</a></div>'
            stringCalendar += '<div class="col-md-4 text-center"><h3>'+ nameMonth +' '+actualYear+'</h3></div>'
            stringCalendar += '<div class="col-md-4 text-center"><a class="btn next-btn" id="nextMonth" href="#">Next</a></div>'
            stringCalendar += '</div> <br>'

        stringCalendar  += '<table class="table">'
        var daysMonth   = this.getMonthDays(actualMonth, actualYear)+1
        var stringWeek  = '<thead><th>L</th><th>M</th><th>M</th><th>J</th><th>V</th><th style="background: gray; color: white;">S</th><th style="background: gray; color: white;">D</th></thead><tbody><tr>'
        stringCalendar  += stringWeek
        var bandera     = false
        var actual      = ""
        var title       = ""
        var eventos     = 0
        for (let index  = 1; index < daysMonth; index++) {
            var weekday = this.getWeekDay(index, actualMonth, actualYear)
            actual      = ""+index+"/"+actualMonth+"/"+actualYear
            if(index < 7 && bandera == false){
                
                if(weekday == 'L'){
                    if(actual == today){
                        title = "Hoy"
                        stringCalendar += "<td title='"+title+"' class='dayActual'>" + index + "</td>"
                    }else{
                        if(eventos == 0){
                            title = "No hay novedades para este día"
                        }else if(eventos == 1){
                            title = "Hay "+ eventos + " novedad para este día"
                        }else if(eventos >= 2){
                            title = "Hay "+ eventos + " novedades para este día"
                        }
                        stringCalendar += "<td title='"+title+"' class='day'>" + index + "</td>"
                    }
                    
                    bandera = true
                }else if(weekday == 'M'){
                    if(actual == today){
                        title = "Hoy"
                        stringCalendar += "<td class='dayActual'>" + index + "</td>"
                    }else{
                        if(eventos == 0){
                            title = "No hay novedades para este día"
                        }else if(eventos == 1){
                            title = "Hay "+ eventos + " novedad para este día"
                        }else if(eventos >= 2){
                            title = "Hay "+ eventos + " novedades para este día"
                        }
                        stringCalendar += "<td>" + "</td>"
                        stringCalendar += "<td title='"+title+"' class='day' onclick='apiCalendar.clickDay("+actual+")'>" + index + "</td>"
                    }
                    bandera = true
                }else if(weekday == 'X'){
                    if(actual == today){
                        title = "Hoy"
                        stringCalendar += "<td class='dayActual'>" + index + "</td>"
                    }else{
                        if(eventos == 0){
                            title = "No hay novedades para este día"
                        }else if(eventos == 1){
                            title = "Hay "+ eventos + " novedad para este día"
                        }else if(eventos >= 2){
                            title = "Hay "+ eventos + " novedades para este día"
                        }
                        stringCalendar += "<td class='day'>" + "</td>"
                        stringCalendar += "<td class='day'>" + "</td>"
                        stringCalendar += "<td title='"+title+"' class='day' onclick='apiCalendar.clickDay("+actual+")'>" + index + "</td>"
                    }
                    bandera = true
                }else if(weekday == 'J'){
                    if(actual == today){
                        title = "Hoy"
                        stringCalendar += "<td class='dayActual'>" + index + "</td>"
                    }else{
                        if(eventos == 0){
                            title = "No hay novedades para este día"
                        }else if(eventos == 1){
                            title = "Hay "+ eventos + " novedad para este día"
                        }else if(eventos >= 2){
                            title = "Hay "+ eventos + " novedades para este día"
                        }
                        stringCalendar += "<td class='day'>" + "</td>"
                        stringCalendar += "<td class='day'>" + "</td>"
                        stringCalendar += "<td class='day'>" + "</td>"
                        stringCalendar += "<td title='"+title+"' class='day' onclick='apiCalendar.clickDay("+actual+")'>" + index + "</td>"
                    }
                    bandera = true
                }else if(weekday == 'V'){
                    if(actual == today){
                        title = "Hoy"
                        stringCalendar += "<td class='dayActual'>" + index + "</td>"
                    }else{
                        if(eventos == 0){
                            title = "No hay novedades para este día"
                        }else if(eventos == 1){
                            title = "Hay "+ eventos + " novedad para este día"
                        }else if(eventos >= 2){
                            title = "Hay "+ eventos + " novedades para este día"
                        }
                        stringCalendar += "<td class='day'>" + "</td>"
                        stringCalendar += "<td class='day'>" + "</td>"
                        stringCalendar += "<td class='day'>" + "</td>"
                        stringCalendar += "<td class='day'>" + "</td>"
                        stringCalendar += "<td title='"+title+"' class='day' onclick='apiCalendar.clickDay("+actual+")'>" + index + "</td>"
                    }
                    bandera = true
                }else if(weekday == 'S'){
                    if(actual == today){
                        title = "Hoy"
                        stringCalendar += "<td class='dayActual'>" + index + "</td>"
                    }else{
                        if(eventos == 0){
                            title = "No hay novedades para este día"
                        }else if(eventos == 1){
                            title = "Hay "+ eventos + " novedad para este día"
                        }else if(eventos >= 2){
                            title = "Hay "+ eventos + " novedades para este día"
                        }
                        stringCalendar += "<td class='day'>" + "</td>"
                        stringCalendar += "<td class='day'>" + "</td>"
                        stringCalendar += "<td class='day'>" + "</td>"
                        stringCalendar += "<td class='day'>" + "</td>"
                        stringCalendar += "<td class='day'>" + "</td>"
                        stringCalendar += "<td title='"+title+"' class='day' onclick='apiCalendar.clickDay("+actual+")'>" + index + "</td>"
                    }
                    bandera = true
                }else if(weekday == 'D'){
                    if(actual == today){
                        title = "Hoy"
                        stringCalendar += "<td class='dayActual'>" + index + "</td>"
                    }else{
                        if(eventos == 0){
                            title = "No hay novedades para este día"
                        }else if(eventos == 1){
                            title = "Hay "+ eventos + " novedad para este día"
                        }else if(eventos >= 2){
                            title = "Hay "+ eventos + " novedades para este día"
                        }
                        stringCalendar += "<td class='day'>" + "</td>"
                        stringCalendar += "<td class='day'>" + "</td>"
                        stringCalendar += "<td class='day'>" + "</td>"
                        stringCalendar += "<td class='day'>" + "</td>"
                        stringCalendar += "<td class='day'>" + "</td>"
                        stringCalendar += "<td class='day'>" + "</td>"
                        stringCalendar += "<td title='"+title+"' class='day'  onclick='apiCalendar.clickDay("+actual+")'>" + index + "</td> </tr><tr>"
                    }
                    bandera = true
                }else{
                    stringCalendar += "<td>" + "</td>"
                }
            }else{
                if(weekday == 'D'){
                    if(actual == today){
                        title = "Hoy"
                        stringCalendar += "<td title='"+title+"' class='dayActual'>" + index + "</td>"
                    }else{
                        if(eventos == 0){
                            title = "No hay novedades para este día"
                        }else if(eventos == 1){
                            title = "Hay "+ eventos + " novedad para este día"
                        }else if(eventos >= 2){
                            title = "Hay "+ eventos + " novedades para este día"
                        }
                        stringCalendar += "<td title='"+title+"' class='day' onclick='apiCalendar.clickDay("+actual+")'>" + index + "</td> </tr><tr>"
                    }
                }else{
                    if(actual == today){
                        title = "Hoy"
                        stringCalendar += "<td title='"+title+"' class='dayActual'>" + index + "</td>"
                    }else{
                        if(eventos == 0){
                            title = "No hay novedades para este día"
                        }else if(eventos == 1){
                            title = "Hay "+ eventos + " novedad para este día"
                        }else if(eventos >= 2){
                            title = "Hay "+ eventos + " novedades para este día"
                        }
                        stringCalendar += "<td title='"+title+"' class='day' onclick='apiCalendar.clickDay("+actual+")'>" + index + "</td>"
                    }
                }
            }
        }
        
        stringCalendar += '</tr></tbody></table>'

        document.getElementById(divSet).innerHTML = stringCalendar

        document.getElementById("prevMonth").onclick= function name(params) {
            if(actualMonth == 1){
                actualMonth = 12
                actualYear = actualYear-1
                apiCalendar.setCalendar(divSet, "", actualMonth, actualYear)
            }else{
                apiCalendar.setCalendar(divSet, "", actualMonth-1, actualYear)
            }
        }
        
        document.getElementById("nextMonth").onclick= function name(params) {
            if(actualMonth == 12){
                actualMonth = 1
                actualYear = actualYear+1
                apiCalendar.setCalendar(divSet, urlAPI, actualMonth,actualYear)
            }else{
                apiCalendar.setCalendar(divSet, urlAPI, actualMonth+1, actualYear)
            }
        }
    }
}


